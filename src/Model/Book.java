package Model;

import java.util.Calendar;
import java.util.Date;

public class Book {
	private String name;
	private int id,studentID;
	private String nameauthor;
	private Date rentDate;
	private int rentStatus;
	
	public Book(String name,int id,String nameauthor){
		this.name =name;
		this.id = id;
		this.nameauthor = nameauthor;
		
	}
	
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public String getNameauthor() {
		return nameauthor;
	}

	public String nameauthor() {
		return nameauthor;
	}
	
	public void setDate(Date d){
		this.rentDate = d;
	}
	
	public void setRentStatus(int status){ // 0 = available
		this.rentStatus = status;
	}
	
	public void setStudentID(int student){
		this.studentID = student;
	}
	
	public int getStudentRentID(){
		int x = 0;
		if(rentStatus == 1){
			x = studentID;
		}
		return x;
	}
}
