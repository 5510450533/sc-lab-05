package Model;

public class Student {
	private String name;
	private int id;
	private int level;
	
	public Student(String name,int id, int level){
		this.name = name;
		this.id = id;
		this.level = level;
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}

}
